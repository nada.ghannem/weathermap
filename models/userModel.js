var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

Schema = mongoose.Schema

const adminSchema = mongoose.model('admin', new mongoose.Schema(
  {
    nom : {
      type : String ,
      required : true ,
      trim : true,
    },
    prenom : {
      type : String ,
      required : true ,
      trim : true,
    },

    email : {
      type : String ,
      required : true ,
      trim : true,
    },
    password: {
      type: String,
      required: true
    },
    photo: {
      type: String,
    },
  }
  )
    .pre("save", function (next) {
      this.password = bcrypt.hashSync(this.password,10 );
      next();
    })
);

module.exports = adminSchema ;
