const userModel = require("../models/userModel")
const bcrypt = require("bcrypt")
const jwt = require('jsonwebtoken')
var fs = require("fs")
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});

module.exports = {

  create: function (req, res) {

    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {
          console.error(err);
          var response = {
            message: 'Sorry, file couldn\'t be uploaded.',
            filename: req.file.originalname
          };
        }
        else {
          response = {
            message: 'File uploaded successfully',
            filename: req.file.originalname
          };



          console.log({
            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email,
            password: req.body.password,
            photo: req.file.originalname

          })


          const user = new userModel({
            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email,
            password: req.body.password,
            photo: req.file.originalname

          })

          user.save(function (err) {

            if (err) {

              console.log(err);
              res.send({state: "error", msg: "error create user"})


            }
            else {

              res.send({state: "create", msg: " create user"})

            }
          })

        }
      })
      })

},


login : function (req, res) {

  userModel.findOne({email: req.body.email}, (function (err, userInfo) {
    if (err)
      next(err);
    else {
      if (userInfo != null) {

        console.log(userInfo)

        if (bcrypt.compare(req.body.password, userInfo.password)) {
          const token = jwt.sign({id: userInfo._id}, req.app.get("secretKey"), {expiresIn: '1h'});
          res.json({status: "success", msg: "user found", data: {user: userInfo, token: token}});
        } else {
          res.json({status: "erreur", msg: "invalid email/pswd", data: null});
        }
      } else {

        res.json({status: "erreur", msg: "vous avez un erreur", data: null});

      }
    }

  }));


},


getImage : function (req, res) {

  console.log(__dirname + "/uploads/" + req.params.photo)
  res.sendFile(__dirname + "/uploads/" + req.params.photo)
}

}
