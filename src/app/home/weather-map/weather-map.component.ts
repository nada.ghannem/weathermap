import {Component, OnInit, ViewChild, ElementRef, NgZone} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {UserService} from "../../service/user.service";
import {from} from "rxjs/index";

import { google } from '@google/maps';

declare var google : any ;
const kelvinToCelsius = from('kelvin-to-celsius');

@Component({
  selector: 'app-weather-map',
  templateUrl: './weather-map.component.html',
  styleUrls: ['./weather-map.component.css']
})
export class WeatherMapComponent implements OnInit {


  // les four propriétés de notre component
  forecast: any[];
  errorMessage: any;
  ville: string;
  disabledForecastbutton: boolean = true;

  google;
  name;
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  vicinity;

  temp;
  humidity;
  temp_min;
  temp_max;

  test: boolean = true;


  @ViewChild('search')
  public searchElementRef: ElementRef;


  constructor(private  userService: UserService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) {


  }


  ngOnInit() {
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();

      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom

          this.vicinity = place.vicinity
          console.log("place", place.vicinity)
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;


          this.userService.recevezLeMeteo(this.vicinity).subscribe(res => {

            this.temp = (JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).main)).temp - 32) / 1.8


            this.humidity = JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).main)).humidity
            this.temp_min = JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).main)).temp_min
            this.temp_max = JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).main)).temp_max


            console.log(this.temp)
            console.log(this.humidity)
            console.log(this.temp_min)
            console.log(this.temp_max)

          })
        });
      });
    });


    /// this.onSubmit()


  }


  delete() {


    this.test = false;

  }


  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  markerDragEnd($event: any) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({'location': {lat: latitude, lng: longitude}}, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }


  // la méthode qui récupère les données émis par le openweather api
  recevezLesDonnees(ville: string) {
    this.userService.recevezLeMeteo(ville).subscribe(data => {
        data = this.forecast;
      },
      error => {
        this.errorMessage = <any>error;
        console.log(error)
      }
    )
  }

  // la methode de la soumission
  onSubmit() {
    this.userService.recevezLeMeteo(this.ville).subscribe(data => {
        data = this.forecast
      },
      error => error = this.errorMessage);
    this.onResetControls()
  }

  //la methode pour performer la recherhce
  onSearch(event: Event) {
    this.ville = (<HTMLInputElement>event.target).value;
    this.disabledForecastbutton = false;
  }

// la méthode pour réinitialiser la forme après soumission
  onResetControls() {
    this.ville = '';
    this.disabledForecastbutton = true;
  }


}
