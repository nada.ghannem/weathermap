import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanDeactivate, CanLoad, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {

  }

  canActivate() {

    if (localStorage .getItem('test') === '1') {

      return true;
    }

    else {
      this.router.navigate(['/login']);
      return false;
    }

  }

  canDeactivate(): any {
    // alert("CanDEactivate called!")
    return false;
  }





}


