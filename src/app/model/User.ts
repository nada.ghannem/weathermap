export class User {

private _nom : string
  private _prenom : string
  private _password : string
  private _email : string
  private _photo : string


  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get photo(): string {
    return this._photo;
  }

  set photo(value: string) {
    this._photo = value;
  }

  constructor(nom: string, prenom: string, password: string, email: string, photo: string) {
    this._nom = nom;
    this._prenom = prenom;
    this._password = password;
    this._email = email;
    this._photo = photo;
  }
}
