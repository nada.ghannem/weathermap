import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './home/sidebar/sidebar.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { MainComponent } from './home/main/main.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { WeatherMapComponent } from './home/weather-map/weather-map.component';
import { WeatherHistoryComponent } from './home/weather-history/weather-history.component';
import {AgmCoreModule} from '@agm/core';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    LoginComponent,
    WeatherMapComponent,
    WeatherHistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBWnLEvgdiPhRHa-fmD6KIDO7PnaJY4cmY',
      // apiKey: 'bdb04a3dfa3659b8239e6cf34d840188',
      libraries: ['places']
    })
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
