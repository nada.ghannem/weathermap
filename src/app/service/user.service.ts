import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({

    providedIn: 'root'


  })

  export class UserService {

  baseUrl = environment.baseUrl;

  APIWeather: 'b1578017bd124add823e2634584d09ae';


  private BaseUrl: string = "https://api.openweathermap.org/data/2.5/";


  constructor(private http: HttpClient) {
  }

  login(email, password) {

    return this.http.post(this.baseUrl + '/user/login', {email: email, password: password})

  }


  recevezLeMeteo(ville) {

    return  this.http.get( this.BaseUrl + 'weather?q='+ville +'&appid='+'b1578017bd124add823e2634584d09ae')



  }
}
