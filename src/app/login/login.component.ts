import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  message;
  loading = false;
  loginForm: FormGroup;
  submit = false;

  email
  pswd ;

  constructor(private router: Router, private formbuilder: FormBuilder, private userService : UserService) {

  }
  ngOnInit() {

    this.loginForm = this.formbuilder.group({

      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })

  }


  get f() {

    return this.loginForm.controls;

  }

  Authentifier() {
    this.submit = true;

    if (this.loginForm.invalid) {



      return
    }
    this.userService.login(this.loginForm.value['email'], this.loginForm.value['password']).subscribe(res => {

        console.log(res);


        console.log(this.loginForm.value['email'])
        console.log(this.loginForm.value['password'])

        if (JSON.parse(JSON.stringify(res)).status === "success") {

          localStorage.setItem('token', JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data.token)));
          localStorage.setItem('user', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).user));
          localStorage.setItem('test', '1');
          this.router.navigate(['/home'])

        } else {

          if (JSON.parse(JSON.stringify(res)).status === "erreur") {
            Swal.fire('Erreur', 'Merci de vérifier votre email et password!', 'error')
            this.loginForm.reset()

          }
        }
      }
    )
  }

}
