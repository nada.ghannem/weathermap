import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './guard/auth.guard';
import {WeatherMapComponent} from './home/weather-map/weather-map.component';
import {WeatherHistoryComponent} from './home/weather-history/weather-history.component';

const routes: Routes = [

  {path : 'login', component : LoginComponent},

  {path : 'home', component : HomeComponent, canActivate : [AuthGuard], children : [

      {path : 'WeatherMap', component : WeatherMapComponent},
      {path : 'WeatherHistory', component : WeatherHistoryComponent}



    ]}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
