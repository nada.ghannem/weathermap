

const userController = require("../controller/userController");
const multer = require('multer');

const upload = multer({dest: __dirname + '/uploads/images'});

const router = require("express").Router();


router.post("/create", upload.single('photo'), userController.create);
router.post("/login", userController.login);
router.get("/getImage/:photo", userController.getImage);


module.exports = router;
