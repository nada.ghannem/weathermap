/*
 module node js
 */

const express = require("express");
const morgan = require("morgan");
var bodyParser = require('body-parser')
var mongoose = require('mongoose');
var cors = require('cors')



/*
 connect to db

 */
const db = require("./models/db")

/*
 router public
 *
 * */

const userRouter = require("./route/userRoute")

/*
 router Privee
 */

const app = express();
app.use(morgan("dev"))


app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(cors('*'))

app.set("secretKey","tokentest")


app.use("/user", userRouter);


app.get("/",   function(req, res) {

  res.send({state: "bienvenue :)"})

})


// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// handle errors
app.use(function (err, req, res, next) {
  console.log(err);

  if (err.status === 404)
    res.status(404).json({message: "Not found"});
  else
    res.status(500).json({message: "Something looks wrong :( !!!"});
});

app.listen(3000,  function () {

  console.log("server run on port 3000")
})
